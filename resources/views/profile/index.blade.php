@extends('layout.master')
@section('judul')
Halaman Profile
@endsection

@section('content')
    <form action="#" method="POST" autocomplete="off">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="{{$profile->user->name}}" class="form-control" disabled>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" value={{$profile->user->email}} class="form-control" disabled>
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" cols="30" rows="3" class="form-control" disabled> {{$profile->alamat}} </textarea>
        </div>
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="biodata" cols="30" rows="5" class="form-control" disabled> {{$profile->bio}} </textarea>
        </div>
        <input type="submit" value="Simpan" disabled>
</form>
@endsection