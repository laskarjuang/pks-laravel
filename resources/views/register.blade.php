@extends('layout.master')
@section('judul')
Halaman Form
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="firstName"><br><br>

        <label>Last Name:</label><br>
        <input type="text" name="lastName"><br><br>

        <label>Gender</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>

        <label>Nationality</label><br>
        <select name="wn" id="">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>

        <label>Language Spoken</label><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br><br>

        <label>Bio</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection