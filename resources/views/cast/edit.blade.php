@extends('layout.master')
@section('judul')
Halaman Edit Cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST" autocomplete="off">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" value={{$cast->umur}} class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror 
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" cols="30" rows="10" class="form-control"> {{$cast->bio}} </textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror 
        <input type="submit" value="Simpan">
        <a href="/cast" class="btn btn-warning btn-sm">Batal</a>
    </form>
@endsection