@extends('layout.master')
@section('judul')
Halaman Tambah Cast
@endsection

@section('content')
    <form action="/cast" method="post" autocomplete="off">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror 
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror 
        <input type="submit" value="Simpan">
        <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
    </form>
@endsection