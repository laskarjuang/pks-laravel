@extends('layout.master')
@section('judul')
Halaman Daftar Cast
@endsection

@section('content')
    @auth
        <a href="/cast/create" class="btn btn-primary btn-sm mb-4"> Tambah Cast</a>    
    @endauth
    <div class = "row">
        @forelse ($cast as $item)
        <div class = "col-4">
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title">{{$item->nama}} ({{$item->umur}} tahun)</h5>
                  <p class="card-text">{{Str::limit($item->bio, 70)}}</p>
                  <a href="/cast/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail</a>
                  
                  @auth
                  <div class="row my-2">
                    <div class = "col">
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                    </div>
                    <div class = "col">
                        <form action="/cast/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-block btn-sm" value="Hapus">
                        </form>
                    </div>
                  </div>
                  @endauth
                </div>
            </div> 
        </div>
        @empty
            <h2>Tidak ada daftar Cast</h2>   
        @endforelse
    </div>

@endsection