@extends('layout.master')
@section('judul')
Halaman Detail Cast
@endsection

@section('content')

    <h5 class="card-title">{{$cast->nama}} ({{$cast->umur}} tahun)</h5>
    <p class="card-text">{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-secondary btn-block btn-sm">Kembali</a>
    
@endsection