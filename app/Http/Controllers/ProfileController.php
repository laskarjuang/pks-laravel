<?php

namespace App\Http\Controllers;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $iduser = Auth::id();
        $profile = Profile::where('user_id', $iduser)->first();
        return view('profile.index',['profile'=>$profile]);
    }
}
